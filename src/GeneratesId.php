<?php

namespace Helium\EloquentId;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

trait GeneratesId
{
	public static function bootGeneratesId()
	{
		self::creating(function (Model $model) {
		    $model->generateId();
		});
	}

	public function getPrefix(): string
	{
		if (isset($this->prefix))
		{
			return $this->prefix;
		}

		$path = explode('\\', static::class);
		$className = array_pop($path);

		return strtoupper(substr($className, 0, 3));
	}

	public function generateId()
    {
        $primaryKey = $this->getKeyName();

				if (!$this->$primaryKey) {
	        $this->$primaryKey = $this->getPrefix() . '-' . Uuid::uuid4()->getHex();
				}
    }
}
